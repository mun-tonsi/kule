// kule - a CSS theming system
// ---------------------------

// Copyright (c) 2023 Genevieve Clifford

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

@font-face {
  font-family: 'Atkinson Hyperlegible';
  font-style: normal;
  src: local(''),
       url('../AtkinsonHyperlegible-Regular.ttf') format('truetype');
}

html {
  font-family: 'Atkinson Hyperlegible', Arial, Helvetica, sans-serif;
}

h1, h2, h3, h4, h5, h6 {
  @extend %headings !optional;
}

// Themes
// ------

// To use these themes, you'll need to set it as an option in the html tag for
// your page, i.e., for light, set: <html lang="en" data-theme="light">

// Light colour theme, based on Solarized Light
html[data-theme='default'],
html[data-theme='light'] {
  --colour-bg: #fdf6e3;
  --colour-text: #657b83;
  --colour-emphasis: #586e75;
  --colour-link: #2aa198;
  --colour-draw: var(--colour-text);
  --draw-width: 1px;
  --colour-headings: #268bd2;
}

// Dark colour theme, based on Solarized Dark
html[data-theme='dark'] {
  --colour-bg: #002b36;
  --colour-text: #839496;
  --colour-emphasis: #93a1a1;
  --colour-link: #2aa198;
  --colour-draw: var(--colour-text);
  --draw-width: 1px;
  --colour-headings: #268bd2;
}

// Dracula colour theme, based on the eponymous colour theme
html[data-theme='dracula'] {
  --colour-bg: #282a36;
  --colour-text: #f8f8f2;
  --colour-emphasis: var(--colour-text);
  --colour-link: #ff79c6;
  --colour-draw: #3a3d4e;
  --draw-width: 2px;
  --colour-headings: #bd93f9;
}

// Textbook colour theme, a simple black-on-white
html[data-theme='textbook'] {
  --colour-bg: white;
  --colour-text: black;
  --colour-emphasis: var(--colour-text);
  --colour-link: var(--colour-text);
  --colour-draw: var(--colour-text);
  --draw-width: 1px;
  --colour-headings: var(--colour-text);
}

// Vampire colour theme, this doesn't look that good, but use it if you like!
html[data-theme='vampire'] {
  --colour-bg: rgb(30, 32, 32);
  --colour-text: rgb(245, 238, 237);
  --colour-emphasis: rgb(234, 41, 19);
  --colour-link: var(--colour-text);
  --colour-draw: var(--colour-text);
  --draw-width: 1px;
  --colour-headings: var(--colour-emphasis);
}

html[data-theme='catppuccin-latte'] {
  --colour-bg: rgb(239, 241, 245);
  --colour-text: rgb(76, 79, 105);
  --colour-emphasis: rgb(114, 135, 253);
  --colour-link: rgb(30, 102, 245);
  --colour-draw: var(--colour-emphasis);
  --draw-width: 2px;
  --colour-headings: rgb(108, 111, 133);
}

html[data-theme='prifysgol'] {
  --colour-bg: white;
  --colour-text: black;
  --colour-emphasis: var(--colour-text);
  --colour-link: rgb(36, 47, 96);
  --colour-draw: var(--colour-link);
  --colour-alternate: rgb(202, 212, 0);
  --draw-width: 1px;
  --colour-headings: var(--colour-draw);
  font-family: Futura, Arial, Helvetica, sans-serif;

  %headings {
    color: var(--colour-headings);
    font-weight: bold;
  }

  a:hover, a.current {
    color: var(--colour-link);
    background-color: var(--colour-alternate);
  }

  h1 {
    text-transform: uppercase;
  }

  footer, nav {
    background-color: var(--colour-draw);
    color: var(--colour-bg);
    a, b, i {
      color: var(--colour-bg);
    }
    
    padding: 4pt;
  }
  
  body {
    width: 90%;
  }
}

body {
    margin: 40px auto;
    width: 80%;
    background: var(--colour-bg);
    color: var(--colour-text);
}

a {
  text-decoration-line: underline;
  text-decoration-style: dotted;
  color: var(--colour-link);
}

a:hover, a.current {
  color: var(--colour-bg);
  background-color: var(--colour-link);
}

%headings {
  color: var(--colour-headings);
  line-height: 1em;
}

h1.logo {
  font-size: 3em;
}

p {
    font-size: 1em;
    line-height: 1.2em;
}

b, i {
  color: var(--colour-emphasis);
}

// Header/footer includes
header, footer {
    font-size: .8em;
}

footer {
  border-top: var(--draw-width) dotted var(--colour-draw);
}

nav {
  border-bottom: var(--draw-width) dotted var(--colour-draw);
  ul {
    padding: 0px;
    list-style: none;
    font-weight: bold;
    li {
      display: inline;
      margin-right: 20px;
    }
  }
}

// tables
table, th, td {
  border: var(--draw-width) solid var(--colour-draw);
}

table {
  width: 100%;
  border-collapse: collapse;
}

// code line wrapping
pre {
  white-space: pre-wrap;       /* css-3 */
  white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
  white-space: -pre-wrap;      /* Opera 4-6 */
  white-space: -o-pre-wrap;    /* Opera 7 */
  word-wrap: break-word;       /* Internet Explorer 5.5+ */
} 

// figure formatting
figure, pre, div.info-box {
  border: 1px dashed;
  border-radius: 5px;
  padding-left: 10px;
  padding-right: 10px;
  margin: auto;
  img {
    max-width: 75%;
    max-height: 50%;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
  margin-top: 10px;
  margin-bottom: 10px;
}

// for the Jekyll figcaption plugin
figcaption {
  border-top: 1px dashed;
  font-size: small;
  font-style: italic;
  padding: 2px;
  text-align: center;
}

// break long links in bibliography (generated by Jekyll's scholar plugin)
ol.bibliography {
  padding-left: 0;
  li {
    list-style: none;
    padding-bottom: 10px;
    span {
      overflow-wrap: break-word;
    }
  }
}

// iframes
iframe.responsive-iframe {
  width: 100%;
  height: 100%;
}

// formatting for sike (my webring software)
div#sike {
  a {
    padding-right: 10px;
  }
}

footer {
  p {
    margin-bottom: 2px;
  }
}