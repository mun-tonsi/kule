# kule

This project contains the stylesheet I maintain for all of my web projects. The source is distributed as [Sass](https://sass-lang.com/) only. A compiled (i.e., CSS) version is available at [https://mun-tonsi.gitlab.io/kule/main.css](https://mun-tonsi.gitlab.io/kule/main.css); this is updated whenever a commit is made to the main branch of this repository. You can include it on your own website by adding the following line to your HTML header:

```html
<link rel="stylesheet" href="https://mun-tonsi.gitlab.io/kule/main.css">
```

## Themes
There are five themes:

- `light` (based on [Solarized](https://ethanschoonover.com/solarized/) Light)
- `dark` (based on [Solarized](https://ethanschoonover.com/solarized/) Dark)
- `dracula` (based on [Dracula](https://draculatheme.com/))
- `textbook` (a simple black + white theme)
- `vampire` (imagine a mid-00s teenage emo GeoCities page, it might look like this)
- `prifysgol` (based on Swansea University's [brand asset guidelines](https://simon.robinson.ac/home/resources/vector-graphics/brand-asset-guidelines.pdf))
- `catppuccin-latte` (based on Catppuccin Latte)

You can set themes by adding options to your `html` tag, i.e., for `light`:

```html
<html lang="en" data-theme="light">
```